import re
import sys
import copy

#TODO: Handle multiple probabilistic effect

ACTION_KEYWORD = ":action"
PROBABILISTIC_KEYWORD = "probabilistic"

# Read the domain and problem file
domain_file = sys.argv[1]
target_file = sys.argv[2]

with open(domain_file) as d_fd:
    original_domain_str = d_fd.read()

# Replace multiple white spaces with a single space
curr_domain_str = re.sub('\(', '( ', original_domain_str)
curr_domain_str = re.sub('\)', ' )', curr_domain_str)
curr_domain_str = re.sub('\n', ' ', curr_domain_str)
curr_domain_str = re.sub('\s\s*', ' ', curr_domain_str)


curr_domain_parts = curr_domain_str.split(' ')

new_domain_parts = []

currently_in_action_defs = False
currently_in_prob_effects = False
currently_in_indiv_prob_effects = False
current_action_parts = [[]]
current_action_stack = []
current_prob_stack = []
current_action_name = None

part_ind = 0
while part_ind < len(curr_domain_parts) - 1:
    part = curr_domain_parts[part_ind]

    # Add new lines in parameter
    if ACTION_KEYWORD in curr_domain_parts[part_ind+1]:
        print ("Starting new action defn")
        currently_in_action_defs = True
        current_action_parts = [[]]
        current_action_parts[-1].append(part)
        current_action_parts[-1].append(curr_domain_parts[part_ind+1])
        current_action_parts[-1].append(curr_domain_parts[part_ind+2])

        current_action_name = curr_domain_parts[part_ind+2]
        current_action_stack.append('(')
        part_ind += 2
        while len(current_action_stack) > 0:
            part_ind += 1
            part = curr_domain_parts[part_ind]
            #print ("act defn", part)

            # Don't add the the opening parenthesis for probability
            if part_ind <  len(curr_domain_parts)-1 and PROBABILISTIC_KEYWORD in curr_domain_parts[part_ind+1]:
                current_prob_stack = []
                current_indiv_effect_stack = []
                #TODO: Currently assuming there is always going to be the possibility that none of the effects happen
                current_effects = [[]]
                current_prob_stack.append('(')
                part_ind += 2
                currently_in_indiv_prob_effects = False
                while len(current_prob_stack) > 0:
                    part_ind += 1
                    part = curr_domain_parts[part_ind]

                    if not currently_in_indiv_prob_effects:
                        if part == '(':
                            currently_in_indiv_prob_effects = True
                            # Dont' add the opening parenthesis
                            current_effects.append([])
                            current_indiv_effect_stack = []
                            current_indiv_effect_stack.append('(')
                        elif part == ')':
                            current_prob_stack.pop()
                    else:
                        if part == '(':
                            current_indiv_effect_stack.append('(')
                        elif part == ')':
                            current_indiv_effect_stack.pop()
                        if len(current_indiv_effect_stack) == 0:
                            currently_in_indiv_prob_effects = False
                        else:
                            current_effects[-1].append(part)

                # Copy the over effects multiple times
                new_action_parts = []
                for curr_act_part in current_action_parts:
                    for eff in current_effects:
                        if eff != []:
                            new_action_parts.append(curr_act_part+['(']+eff+[')'])
                        else:
                            new_action_parts.append(curr_act_part + ['(and )'])
                current_action_parts = copy.deepcopy(new_action_parts)
            else:
                for ind in range(len(current_action_parts)):
                    current_action_parts[ind].append(part)
                if part == '(':
                    current_action_stack.append('(')
                elif part == ')':
                    current_action_stack.pop()
                #if part == ')':
                #    current_action_stack.pop()

        #print ("we got here")
        # Copy all the current_action_parts to new_domain_parts (after replacing the name)z
        print(current_action_parts)

        act_ind = 0
        for act_defn_parts in current_action_parts:
            act_defn = " ".join(act_defn_parts)
            act_defn = act_defn.replace(current_action_name, current_action_name + str(act_ind))
            act_ind += 1
            new_domain_parts.append(act_defn)
    else:
        new_domain_parts.append(part)
    part_ind += 1

new_domain_parts.append(curr_domain_parts[-1])
new_domain_str = " ".join(new_domain_parts)

new_domain_str = new_domain_str.replace('( :action', '\n( :action')
new_domain_str = new_domain_str.replace('( :requirements', '\n( :requirements')
new_domain_str = new_domain_str.replace('( :types', '\n( :types')
new_domain_str = new_domain_str.replace('( :predicates', '\n( :predicates')
new_domain_str = new_domain_str.replace(':probabilistic-effects ', '')


with open(target_file,'w') as t_fd:
    t_fd.write(new_domain_str)