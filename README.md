# Policy Summarization

Please use FD_BASED_LANDGEN branch for the latest code

## Full summarization flow
1. If domain contains quantified preconditions/effects then ground the domain (tarski or pr2domain)
2. Generate policy using MDP-lib (updated)
3. Determinize the domain using the determinization script
4. Make the policy specific domain
5. Use the landmark script to extract landmarks
