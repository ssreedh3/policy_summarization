;;;;;;;;;;;;;;;;;;;;;
;; Start at home
;; Try to catch a train
;; Trains could breakdown
;; If I miss train or train brokedown, take a taxi
;; if train then get down at the train station and walk to airport
;; At airport buy a ticket to sf and board the plane
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain get_to_sf)
  (:requirements :probabilistic-effects :conditional-effects :negative-preconditions :equality :typing)
  (:types elevator floor pos coin)
  (:constants f1 - floor p1 - pos)
  (:predicates (at_home) (at_station1) (at_station2) (at_station3) (at_airport) (on_board_train) (missed_train) (has_ticket) (boarded_plane) )

  (:action walk_to_train_station
    :parameters ()
    :precondition (and (at_home) )
    :effect (and (at_station1) (not (at_home)))
  )
  (:action walk_to_airport
    :parameters ()
    :precondition (and (at_station3))
    :effect (and (at_airport) (not (at_station3)))
  )

  (:action catch_train
    :parameters ()
    :precondition (and (at_station1) )
    :effect (probabilistic 1/2 (and (not (at_station1)) (on_board_train))
                           1/2 (and (missed_train))))

  (:action ride_train
    :parameters ()
    :precondition (and (on_board_train) )
    :effect (and (not (on_board_train))
				(probabilistic 1/2 (and (at_station3))
                           1/2 (and (at_station2)))
			))

  (:action take_taxi1
    :parameters ()
    :precondition (and (at_station1) (missed_train) )
    :effect  (and (at_airport) (not (at_station1))))

  (:action take_taxi2
    :parameters ()
    :precondition (and (at_station2) )
    :effect  (and (at_airport) (not (at_station1))))

  (:action buy_ticket
    :parameters ()
    :precondition (and (at_airport) )
    :effect (and (has_ticket) )
  )
  (:action board_flight
    :parameters ()
    :precondition (and (has_ticket))
    :effect (and (boarded_plane) ))
)
