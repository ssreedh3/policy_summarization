import re
import copy

def parse_policy_lines(policy_line):
    policy_line_reg = re.compile("\[(.*)\] \((.*)\)")
    state_str = policy_line_reg.match(policy_line).group(1)
    act = policy_line_reg.match(policy_line).group(2).replace(' ','_')
    state_set = set()
    for pred in state_str.split(' ,'):
        if pred != '':
            state_set.add(pred.replace('(','').replace(')','').strip())
    return act, state_set

def ground_predicate(orig_pred, var_map):
    new_pred = orig_pred
    for var in var_map:
        #Add an escape character to ?
        new_pred = new_pred.replace(var, var_map[var])
    return new_pred


def ground_action(action_map, var_map):
    grounded_action = copy.deepcopy(action_map)

    # ground preconditions
    grounded_action['precondition'] = set([ground_predicate(pred, var_map) for pred in action_map['precondition']])

    # ground conditional add effects
    grounded_action['conditional_add_effects'] = []
    for cond, eff in action_map['conditional_add_effects']:
        grounded_cond = [ground_predicate(pred, var_map) for pred in cond]
        grounded_eff = ground_predicate(eff, var_map)
        grounded_action['conditional_add_effects'].append([grounded_cond, grounded_eff])


    # ground conditional delete effects
    grounded_action['conditional_del_effects'] = []
    for cond, eff in action_map['conditional_del_effects']:
        grounded_cond = [ground_predicate(pred, var_map) for pred in cond]
        grounded_eff = ground_predicate(eff, var_map)
        grounded_action['conditional_del_effects'].append([grounded_cond, grounded_eff])

    # ground add effects
    grounded_action['add_effects'] = set([ground_predicate(pred, var_map) for pred in action_map['add_effects']])

    # ground delete effects
    grounded_action['del_effects'] = set([ground_predicate(pred, var_map) for pred in action_map['del_effects']])


    return grounded_action


def test_condition(state, condition):
    non_equality_preds = set()
    for pred in condition:
        if '=' in pred:
            pred_parts = pred.split(' ')
            assert len(pred_parts) == 3
            if pred_parts[1] != pred_parts[2]:
                return False
        else:
            non_equality_preds.add(pred)
    if non_equality_preds <= state:
        return True
    return False
