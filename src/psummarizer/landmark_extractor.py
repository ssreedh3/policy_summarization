import copy

MAX_UNROLL_LIMIT = 100

# TODOs:
# 1. Don't worry about the ordering for now -- Done
# 1.1 Restrict orderings to just facts -- Done
# 2. Don't worry about conjuncts for now -- Maybe
# 3. Don't worry about tarski for now -- Done
# 4. Extend to handle disjunctions

# key for conjucts
def get_conjunct_key_str(pred_set):
    return ",".join(sorted(list(pred_set)))

# Union for actions
def get_union_of_landmarks(act, precondition_set, greedy_necc_ordering_map, label_map):
    '''
    greedy_necc_ordering_map consists of a dict which contains map of type
    fact -> preceding facts
    :param act: curr_act_name + (indexed with level number)
    :param precondition_set: The set of actions preconditions
    :param greedy_necc_ordering_map: the current dictionary containing landmarks and ordering
    :param label_map: the current dictionary containing landmarks and ordering
    :return greedy_necc_ordering_map, label_map
    '''
    # Standard method would involve finding the union of landmark for every fact in the precondition
    # Here if action is not part of greedy_necc_ordering_map add a mapping from action to precondition set

    greedy_necc_ordering_map[act] = set()
    curr_set = set()
    for f in precondition_set:
        curr_set |= label_map[f]
    if len(precondition_set) > 0:
        label_map[act] = curr_set | set([get_conjunct_key_str(precondition_set)])
        greedy_necc_ordering_map[act].add(get_conjunct_key_str(precondition_set))
    else:
        label_map[act] = curr_set
        greedy_necc_ordering_map[act] = curr_set
    return greedy_necc_ordering_map, label_map


# Intersection for facts
def get_intersection_of_landmarks(fact_name, pred_actions, greedy_necc_ordering_map, label_map):
    '''

    '''
    # Original Zhu method
    pred_actions_list = list(pred_actions)

    curr_set = set()
    conjunctive_list = []

    for id in range(len(pred_actions_list)):
        ac = pred_actions_list[id]
        for fc in label_map[ac]:
            if ',' in fc:
                conjunctive_list.append(fc)
        if id == 0:
            curr_set = copy.deepcopy(label_map[ac])
        curr_set &= label_map[ac]

    # Figure out what subset of conjunction is preserved
    preserved_conjuncts = set()
    facts_in_conjuncts = set()

    # Split the conjunction and see the intersection
    for conj in conjunctive_list:
        curr_fact_set = set([i for i in conj.split(',')])
        curr_fact_set = curr_fact_set & curr_set
        # get a new conjunct and add
        preserved_conjuncts.add(get_conjunct_key_str(curr_fact_set))
        for fact in curr_fact_set:
            facts_in_conjuncts.add(fact)


    curr_set.add(fact_name)
    label_map[fact_name] = curr_set | preserved_conjuncts
    greedy_necc_ordering_map[fact_name] = set()
    for fact in label_map[fact_name]:
        if ',' not in fact and fact != fact_name and fact not in facts_in_conjuncts:
            greedy_necc_ordering_map[fact_name].add(fact)
    for conj in preserved_conjuncts:
        greedy_necc_ordering_map[fact_name].add(conj)
    return greedy_necc_ordering_map, label_map

 
    


# Set the landmarks
def get_landmarks(init, goal, model):
    '''
       :params init - A set for initial state
       :params goal - Goal specifications set
       :params model - A dictionary containing grounded action precondition and effects
    '''
    current_state = init

    #TODO: Also save the natural ordering
    #natural_ordering_map = {}

    #TODO: Right now just focus on the greedy necc ordering
    greedy_necc_ordering_map = {}

    fixed_point_reached = False
    curr_level = 1

    all_possible_facts = set()
    already_found_actions = set()
    already_found_facts = set()
    label_map = {}
    for fact in current_state:
        label_map[fact] = set([fact])
        already_found_facts.add(fact)

    goal_reached = False
    while (not fixed_point_reached and not goal_reached and curr_level < MAX_UNROLL_LIMIT):
        # Look for applicable action

        new_state = copy.deepcopy(current_state)
        new_facts = set()
        fact_to_act_map = {}
        curr_level += 1
        for act in model:
            preconditions = model[act]['precondition']
            if preconditions <= current_state:
                # For each action that is applicable make an intersection of landmarks
                if act not in already_found_actions:
                    greedy_necc_ordering_map, label_map = get_union_of_landmarks(act, preconditions, greedy_necc_ordering_map, label_map)
                    already_found_actions.add(act)

                for fact in model[act]['add_effects']:
                    new_state.add(fact)
                    if fact not in already_found_facts:
                        if fact not in fact_to_act_map:
                            fact_to_act_map[fact] = set()
                        fact_to_act_map[fact].add(act)

        # Get fact landmarks
        for fact in fact_to_act_map:
            greedy_necc_ordering_map, label_map = get_intersection_of_landmarks(fact, fact_to_act_map[fact], greedy_necc_ordering_map, label_map)
            already_found_facts.add(fact)

        if current_state == new_state:
            fixed_point_reached = True

        # Copy over new state to current state
        current_state = new_state

        if goal <= current_state:
            goal_reached = True

        all_possible_facts = all_possible_facts | new_facts
        #print ("current_state", current_state)
        #print ("actions applied", already_found_actions)
        #for act in already_found_actions:
        #    print (act, model[act]['precondition'])
        #print ("greedy_necc_ordering_map", greedy_necc_ordering_map)
        #if curr_level == 3:
    #print ("greedy_necc_ordering_map", greedy_necc_ordering_map)
    #exit(0)

    for key in greedy_necc_ordering_map:
        for k_p in key.split(','):
            for par in greedy_necc_ordering_map[k_p]:
                if k_p in par.split(','):
                    print ("LOOPY LOOPY LOOPS", k_p, par)
                    exit(0)

    print ("No loops")
    #exit(0)
    return greedy_necc_ordering_map, label_map
