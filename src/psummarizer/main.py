import sys
import copy
from psummarizer.model_parser import parse_model
from psummarizer.landmark_extractor import get_landmarks
from psummarizer.utils import parse_policy_lines, ground_action, test_condition

domain_file = sys.argv[1]
problem_file = sys.argv[2]
policy_file = sys.argv[3]

# TODO: Check to make sure its fully grounded (or ground it if required)
init, goal, model = parse_model(domain_file, problem_file)

uniq_act_id = 0
new_model = {}

new_goal_pred = "goal_completed"
new_goal_set =  set([new_goal_pred])
with open(policy_file) as p_fd:
    for line in p_fd.readlines():
        policy_pair = parse_policy_lines(line)
        curr_id = 0
        act_name_parts = policy_pair[0].split('_')
        curr_act_name = policy_pair[0]
        if act_name_parts[0] == "goal-act":
            new_model['goal-act'+str(uniq_act_id)] = {'precondition': policy_pair[1]}
            new_model['goal-act' + str(uniq_act_id)]['add_effects'] = new_goal_set
            uniq_act_id += 1
        else:
            while act_name_parts[0]+str(curr_id) in model:
                original_act = copy.deepcopy(model[act_name_parts[0]+str(curr_id)])
                var_to_obj_map = {}

                # Map parameters to objects
                for v_ind in range(len(original_act["parameters"])):
                    var_to_obj_map[original_act["parameters"][v_ind]] = act_name_parts[v_ind + 1]


                # Ground the action
                grounded_action = ground_action(original_act, var_to_obj_map)


                new_model[curr_act_name + str(uniq_act_id)] = {'precondition': policy_pair[1]}
                # Add add effects and del effects
                new_model[curr_act_name + str(uniq_act_id)]['add_effects'] = grounded_action['add_effects']
                new_model[curr_act_name + str(uniq_act_id)]['del_effects'] = grounded_action['del_effects']

                # Go through the conditional effects
                for cond, eff in grounded_action['conditional_add_effects']:
                    if test_condition(policy_pair[1], cond):
                        new_model[curr_act_name + str(uniq_act_id)]['add_effects'].add(eff)

                for cond, eff in grounded_action['conditional_del_effects']:
                    if test_condition(policy_pair[1], cond):
                        new_model[curr_act_name + str(uniq_act_id)]['del_effects'].add(eff)

                uniq_act_id += 1
                curr_id += 1

# Call the landmark extractors
gn_ordering, labels = get_landmarks(init, new_goal_set, new_model)

landmark_layers = [set(['goal_completed'])]
curr_parents = gn_ordering['goal_completed']

while len(curr_parents) > 0:
    new_parents = set()
    for land in curr_parents:
        if ',' in land:
            curr_land_parts = land.split(',')
        else:
            curr_land_parts = [land]
        for indiv_lnd in curr_land_parts:
            if indiv_lnd in gn_ordering:
                #print(land, indiv_lnd, gn_ordering[indiv_lnd])
                for par in gn_ordering[indiv_lnd]:
                    new_parents.add(par)
            # else:
            #    print (indiv_lnd,"not in gn_ordering")
    #print("size of new_parents", len(new_parents), new_parents)
    curr_layer = curr_parents - new_parents
    #print ("ORIGINAL LAYER", curr_layer)
    # TODO: Deduplicate
    dedup_list = []
    for indiv_conj in curr_layer:
        indiv_conj_set = set(indiv_conj.split(','))
        has_superset = False
        has_subset = False
        subset_id = -1
        for curr_item_id in range(len(dedup_list)):
            curr_item = dedup_list[curr_item_id]
            if indiv_conj_set <= curr_item:
                has_superset =True
            elif curr_item < indiv_conj_set:
                has_subset =True
                subset_id = curr_item_id
        if not has_superset:
            if has_subset:
                dedup_list[curr_item_id] = indiv_conj_set
            else:
                dedup_list.append(indiv_conj_set)
    #print ("DEDUPED LIST", dedup_list)
    dedup_layer = []
    for curr_conj_set in dedup_list:
        dedup_layer.append(','.join(list(curr_conj_set)))
    landmark_layers.append(dedup_layer)
    remaining_parents = set()
    for par in curr_parents:
        if par in new_parents:
            remaining_parents.add(par)
    curr_parents = copy.deepcopy(remaining_parents)
    #print("curr_parents", curr_parents)

for layer in landmark_layers:
    print ("layers", layer)