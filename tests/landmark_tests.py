import copy
from psummarizer.landmark_extractor import get_landmarks

FACT_LANDMARK_TEST = {'init':set(['p']), 'goal': set(['g']),
                      'model':{'a1':{'precondition':set(['p']), 'add_effects': set(['q'])},
                               'a2':{'precondition':set(), 'add_effects': set(['q'])},
                               'a3':{'precondition':set(['q']), 'add_effects': set(['g'])}}}

CONJUNCTIVE_LANDMARK_TEST = {'init':set(['p','q','r','s']), 'goal': set(['g']),
                             'model':{'a1':{'precondition':set(['p','q','r']), 'add_effects': set(['t'])},
                               'a2':{'precondition':set(['q','r','s']), 'add_effects': set(['t'])},
                               'a3':{'precondition':set(['t']), 'add_effects': set(['g'])}}}

POLICY_LANDMARK_TEST = {'init':set(['p']), 'goal': set(['g']),
                             'model':{'a1_1':{'precondition':set(['p']), 'add_effects': set(['q'])},
                                      'a1_2':{'precondition':set(['p']), 'add_effects': set()},
                                      'a2_1':{'precondition':set(['p','q']), 'add_effects': set(['r'])},
                                      'a2_2': {'precondition': set(['p', 'q']), 'add_effects': set(['s'])},
                                      'a3':{'precondition':set(['p', 'q', 'r']), 'add_effects': set(['g'])},
                                      'a4': {'precondition': set(['p', 'q', 's']), 'add_effects': set(['g'])}
                                      }}

def test_conjunctive_landmarks():
    g_ordering, labels = get_landmarks(CONJUNCTIVE_LANDMARK_TEST['init'],
                                       CONJUNCTIVE_LANDMARK_TEST['goal'], CONJUNCTIVE_LANDMARK_TEST['model'])
    assert labels['t'] == set(['t', 'q,r', 'q', 'r'])
    assert g_ordering['g'] == set(['t', 'q,r'])
    assert g_ordering['t'] == set(['q,r'])



def test_fact_conjunctive():
    g_ordering, labels = get_landmarks(FACT_LANDMARK_TEST['init'],
                                       FACT_LANDMARK_TEST['goal'], FACT_LANDMARK_TEST['model'])
    assert labels['g'] == set(['q', 'g'])
    assert g_ordering['g'] == set(['q'])

def test_sample_policy():
    g_ordering, labels = get_landmarks(POLICY_LANDMARK_TEST['init'],
                                       POLICY_LANDMARK_TEST['goal'], POLICY_LANDMARK_TEST['model'])
    #raise Exception(g_ordering)

