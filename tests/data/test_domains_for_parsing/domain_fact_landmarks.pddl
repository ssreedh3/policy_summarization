(define (domain dummy)
  (:requirements :strips :typing :equality)
  (:predicates (p1) (p2) (p3) (goal))
  (:action dummy0 :parameters () :precondition (and (p1)) :effect (and (goal) (when (p2) (and (p3)))))
  (:action dummy1 :parameters () :precondition (and (p1)) :effect (and (goal) ))
)
