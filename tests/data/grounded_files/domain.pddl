(define
	(domain grounded-ZENOTRAVEL)
	(:requirements :strips)
	(:predicates
		( FLYING_A1_C0 )
		( FLYING_A1_C1 )
		( FLYING_A1_C2 )
		( FLYING_A1_C3 )
		( FLYING_A1_C4 )
		( FLYING_A1_C5 )
		( FLYING_A0_C0 )
		( FLYING_A0_C1 )
		( FLYING_A0_C2 )
		( FLYING_A0_C3 )
		( FLYING_A0_C4 )
		( FLYING_A0_C5 )
		( FUEL-LEVEL_A1_F3 )
		( AT-AIRCRAFT_A1_C1 )
		( AT-AIRCRAFT_A1_C2 )
		( AT-AIRCRAFT_A1_C3 )
		( AT-AIRCRAFT_A1_C4 )
		( AT-AIRCRAFT_A1_C5 )
		( FUEL-LEVEL_A1_F2 )
		( FUEL-LEVEL_A1_F1 )
		( AT-AIRCRAFT_A0_C0 )
		( FUEL-LEVEL_A0_F0 )
		( AT-AIRCRAFT_A0_C2 )
		( AT-AIRCRAFT_A0_C3 )
		( AT-AIRCRAFT_A0_C4 )
		( AT-AIRCRAFT_A0_C5 )
		( FUEL-LEVEL_A1_F0 )
		( ZOOMING_A1_C0 )
		( ZOOMING_A1_C1 )
		( ZOOMING_A1_C2 )
		( ZOOMING_A1_C3 )
		( ZOOMING_A1_C4 )
		( ZOOMING_A1_C5 )
		( REFUELING_A1 )
		( REFUELING_A0 )
		( FUEL-LEVEL_A0_F2 )
		( BOARDING_P0_A0 )
		( BOARDING_P0_A1 )
		( BOARDING_P1_A0 )
		( BOARDING_P1_A1 )
		( IN_P0_A0 )
		( IN_P0_A1 )
		( IN_P1_A0 )
		( IN_P1_A1 )
		( DEBARKING_P0_A0 )
		( DEBARKING_P0_A1 )
		( DEBARKING_P1_A0 )
		( DEBARKING_P1_A1 )
		( AT-PERSON_P0_C0 )
		( AT-PERSON_P0_C1 )
		( AT-PERSON_P0_C2 )
		( AT-PERSON_P0_C4 )
		( AT-PERSON_P0_C5 )
		( AT-PERSON_P1_C0 )
		( AT-PERSON_P1_C1 )
		( AT-PERSON_P1_C2 )
		( AT-PERSON_P1_C3 )
		( AT-PERSON_P1_C5 )
		( ZOOMING_A0_C0 )
		( ZOOMING_A0_C1 )
		( ZOOMING_A0_C2 )
		( ZOOMING_A0_C3 )
		( ZOOMING_A0_C4 )
		( ZOOMING_A0_C5 )
		( FUEL-LEVEL_A0_F3 )
		( FUEL-LEVEL_A0_F4 )
		( AT-AIRCRAFT_A0_C1 )
		( NOT-REFUELING_A0 )
		( NOT-BOARDING_P1 )
		( NOT-BOARDING_P0 )
		( AT-AIRCRAFT_A1_C0 )
		( NOT-DEBARKING_P1 )
		( NOT-DEBARKING_P0 )
		( AT-PERSON_P1_C4 )
		( AT-PERSON_P0_C3 )
		( FUEL-LEVEL_A0_F1 )
		( NOT-REFUELING_A1 )
		( FUEL-LEVEL_A1_F4 )
		( EXPLAINED_FULL_OBS_SEQUENCE )
		( NOT_EXPLAINED_FULL_OBS_SEQUENCE )
	) 
	(:action COMPLETE-ZOOMING0_A0_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( ZOOMING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F2 )
			(not ( ZOOMING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( ZOOMING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F2 )
			(not ( ZOOMING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( ZOOMING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F2 )
			(not ( ZOOMING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( ZOOMING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F2 )
			(not ( ZOOMING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( ZOOMING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F2 )
			(not ( ZOOMING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( ZOOMING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F2 )
			(not ( ZOOMING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( FLYING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F3 )
			(not ( FLYING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( FLYING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F3 )
			(not ( FLYING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( FLYING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F3 )
			(not ( FLYING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( FLYING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F3 )
			(not ( FLYING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( FLYING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F3 )
			(not ( FLYING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F4 )
			( FLYING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F3 )
			(not ( FLYING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F4 ))
		)
	)
	(:action START-FLYING0_A0_C5_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C4_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C3_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C2_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C1_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C0_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action COMPLETE-REFULING0_A0_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( REFUELING_A0 )
		)
		:effect
		(and
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F4 )
			(not ( REFUELING_A0 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action START-REFUELING0_A0_C5_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C4_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C3_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C2_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C1_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C0_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( ZOOMING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F1 )
			(not ( ZOOMING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( ZOOMING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F1 )
			(not ( ZOOMING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( ZOOMING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F1 )
			(not ( ZOOMING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( ZOOMING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F1 )
			(not ( ZOOMING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( ZOOMING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F1 )
			(not ( ZOOMING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( ZOOMING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F1 )
			(not ( ZOOMING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( FLYING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F2 )
			(not ( FLYING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( FLYING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F2 )
			(not ( FLYING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( FLYING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F2 )
			(not ( FLYING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( FLYING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F2 )
			(not ( FLYING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( FLYING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F2 )
			(not ( FLYING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F3 )
			( FLYING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F2 )
			(not ( FLYING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F3 ))
		)
	)
	(:action START-FLYING0_A0_C5_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C4_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C3_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C2_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C1_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C0_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-BOARDING0_P1_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( AT-PERSON_P1_C5 )
		)
		:effect
		(and
			( BOARDING_P1_A1 )
			(not ( AT-PERSON_P1_C5 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( AT-PERSON_P1_C3 )
		)
		:effect
		(and
			( BOARDING_P1_A1 )
			(not ( AT-PERSON_P1_C3 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( AT-PERSON_P1_C2 )
		)
		:effect
		(and
			( BOARDING_P1_A1 )
			(not ( AT-PERSON_P1_C2 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( AT-PERSON_P1_C1 )
		)
		:effect
		(and
			( BOARDING_P1_A1 )
			(not ( AT-PERSON_P1_C1 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( AT-PERSON_P1_C0 )
		)
		:effect
		(and
			( BOARDING_P1_A1 )
			(not ( AT-PERSON_P1_C0 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( AT-PERSON_P1_C5 )
		)
		:effect
		(and
			( BOARDING_P1_A0 )
			(not ( AT-PERSON_P1_C5 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( AT-PERSON_P1_C3 )
		)
		:effect
		(and
			( BOARDING_P1_A0 )
			(not ( AT-PERSON_P1_C3 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( AT-PERSON_P1_C2 )
		)
		:effect
		(and
			( BOARDING_P1_A0 )
			(not ( AT-PERSON_P1_C2 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( AT-PERSON_P1_C1 )
		)
		:effect
		(and
			( BOARDING_P1_A0 )
			(not ( AT-PERSON_P1_C1 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( AT-PERSON_P1_C0 )
		)
		:effect
		(and
			( BOARDING_P1_A0 )
			(not ( AT-PERSON_P1_C0 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P0_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( AT-PERSON_P0_C5 )
		)
		:effect
		(and
			( BOARDING_P0_A1 )
			(not ( AT-PERSON_P0_C5 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( AT-PERSON_P0_C4 )
		)
		:effect
		(and
			( BOARDING_P0_A1 )
			(not ( AT-PERSON_P0_C4 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( AT-PERSON_P0_C2 )
		)
		:effect
		(and
			( BOARDING_P0_A1 )
			(not ( AT-PERSON_P0_C2 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( AT-PERSON_P0_C1 )
		)
		:effect
		(and
			( BOARDING_P0_A1 )
			(not ( AT-PERSON_P0_C1 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( AT-PERSON_P0_C0 )
		)
		:effect
		(and
			( BOARDING_P0_A1 )
			(not ( AT-PERSON_P0_C0 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( AT-PERSON_P0_C5 )
		)
		:effect
		(and
			( BOARDING_P0_A0 )
			(not ( AT-PERSON_P0_C5 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( AT-PERSON_P0_C4 )
		)
		:effect
		(and
			( BOARDING_P0_A0 )
			(not ( AT-PERSON_P0_C4 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( AT-PERSON_P0_C2 )
		)
		:effect
		(and
			( BOARDING_P0_A0 )
			(not ( AT-PERSON_P0_C2 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( AT-PERSON_P0_C1 )
		)
		:effect
		(and
			( BOARDING_P0_A0 )
			(not ( AT-PERSON_P0_C1 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( AT-PERSON_P0_C0 )
		)
		:effect
		(and
			( BOARDING_P0_A0 )
			(not ( AT-PERSON_P0_C0 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action COMPLETE-REFULING0_A0_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( REFUELING_A0 )
		)
		:effect
		(and
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F3 )
			(not ( REFUELING_A0 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action START-REFUELING0_A0_C5_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C4_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C3_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C2_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C1_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C0_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( ZOOMING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F0 )
			(not ( ZOOMING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( ZOOMING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F0 )
			(not ( ZOOMING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( ZOOMING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F0 )
			(not ( ZOOMING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( ZOOMING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F0 )
			(not ( ZOOMING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( ZOOMING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F0 )
			(not ( ZOOMING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A0_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( ZOOMING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F0 )
			(not ( ZOOMING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C5_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C4_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C3_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C2_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C1_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-ZOOMING0_A0_C0_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( ZOOMING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( FLYING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F1 )
			(not ( FLYING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( FLYING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F1 )
			(not ( FLYING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( FLYING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F1 )
			(not ( FLYING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( FLYING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F1 )
			(not ( FLYING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( FLYING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F1 )
			(not ( FLYING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F2 )
			( FLYING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F1 )
			(not ( FLYING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F2 ))
		)
	)
	(:action START-FLYING0_A1_C5_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C4_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C3_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C2_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C1_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C0_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A0_C5_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C4_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C3_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C2_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C0_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A1_C5_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C4_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C3_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C2_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C1_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C0_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A0_C5_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C5_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C5 ))
		)
	)
	(:action START-FLYING0_A0_C4_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C4_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C4 ))
		)
	)
	(:action START-FLYING0_A0_C3_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C3_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C3 ))
		)
	)
	(:action START-FLYING0_A0_C2_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C2_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C2 ))
		)
	)
	(:action START-FLYING0_A0_C1_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C0_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A0_C0_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C0 ))
		)
	)
	(:action START-FLYING0_A1_C5_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C4_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C3_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C2_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C1_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C0_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C5_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C5_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-FLYING0_A1_C4_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C4_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-FLYING0_A1_C3_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C3_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-FLYING0_A1_C2_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C2_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-FLYING0_A1_C1_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-FLYING0_A1_C1_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( DEBARKING_P1_A1 )
		)
		:effect
		(and
			( AT-PERSON_P1_C5 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( DEBARKING_P1_A1 )
		)
		:effect
		(and
			( AT-PERSON_P1_C4 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( DEBARKING_P1_A1 )
		)
		:effect
		(and
			( AT-PERSON_P1_C3 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( DEBARKING_P1_A1 )
		)
		:effect
		(and
			( AT-PERSON_P1_C2 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( DEBARKING_P1_A1 )
		)
		:effect
		(and
			( AT-PERSON_P1_C1 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( DEBARKING_P1_A1 )
		)
		:effect
		(and
			( AT-PERSON_P1_C0 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( DEBARKING_P1_A0 )
		)
		:effect
		(and
			( AT-PERSON_P1_C5 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( DEBARKING_P1_A0 )
		)
		:effect
		(and
			( AT-PERSON_P1_C4 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( DEBARKING_P1_A0 )
		)
		:effect
		(and
			( AT-PERSON_P1_C3 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( DEBARKING_P1_A0 )
		)
		:effect
		(and
			( AT-PERSON_P1_C2 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( DEBARKING_P1_A0 )
		)
		:effect
		(and
			( AT-PERSON_P1_C1 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P1_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( DEBARKING_P1_A0 )
		)
		:effect
		(and
			( AT-PERSON_P1_C0 )
			( NOT-DEBARKING_P1 )
			(not ( DEBARKING_P1_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( DEBARKING_P0_A1 )
		)
		:effect
		(and
			( AT-PERSON_P0_C5 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( DEBARKING_P0_A1 )
		)
		:effect
		(and
			( AT-PERSON_P0_C4 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( DEBARKING_P0_A1 )
		)
		:effect
		(and
			( AT-PERSON_P0_C3 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( DEBARKING_P0_A1 )
		)
		:effect
		(and
			( AT-PERSON_P0_C2 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( DEBARKING_P0_A1 )
		)
		:effect
		(and
			( AT-PERSON_P0_C1 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( DEBARKING_P0_A1 )
		)
		:effect
		(and
			( AT-PERSON_P0_C0 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A1 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( DEBARKING_P0_A0 )
		)
		:effect
		(and
			( AT-PERSON_P0_C5 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( DEBARKING_P0_A0 )
		)
		:effect
		(and
			( AT-PERSON_P0_C4 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( DEBARKING_P0_A0 )
		)
		:effect
		(and
			( AT-PERSON_P0_C3 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( DEBARKING_P0_A0 )
		)
		:effect
		(and
			( AT-PERSON_P0_C2 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( DEBARKING_P0_A0 )
		)
		:effect
		(and
			( AT-PERSON_P0_C1 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A0 ))
		)
	)
	(:action COMPLETE-DEBARKING0_P0_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( DEBARKING_P0_A0 )
		)
		:effect
		(and
			( AT-PERSON_P0_C0 )
			( NOT-DEBARKING_P0 )
			(not ( DEBARKING_P0_A0 ))
		)
	)
	(:action START-DEBARKING0_P1_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( IN_P1_A1 )
		)
		:effect
		(and
			( DEBARKING_P1_A1 )
			(not ( IN_P1_A1 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( IN_P1_A1 )
		)
		:effect
		(and
			( DEBARKING_P1_A1 )
			(not ( IN_P1_A1 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( IN_P1_A1 )
		)
		:effect
		(and
			( DEBARKING_P1_A1 )
			(not ( IN_P1_A1 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( IN_P1_A1 )
		)
		:effect
		(and
			( DEBARKING_P1_A1 )
			(not ( IN_P1_A1 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( IN_P1_A1 )
		)
		:effect
		(and
			( DEBARKING_P1_A1 )
			(not ( IN_P1_A1 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( IN_P1_A1 )
		)
		:effect
		(and
			( DEBARKING_P1_A1 )
			(not ( IN_P1_A1 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( IN_P1_A0 )
		)
		:effect
		(and
			( DEBARKING_P1_A0 )
			(not ( IN_P1_A0 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( IN_P1_A0 )
		)
		:effect
		(and
			( DEBARKING_P1_A0 )
			(not ( IN_P1_A0 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( IN_P1_A0 )
		)
		:effect
		(and
			( DEBARKING_P1_A0 )
			(not ( IN_P1_A0 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( IN_P1_A0 )
		)
		:effect
		(and
			( DEBARKING_P1_A0 )
			(not ( IN_P1_A0 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( IN_P1_A0 )
		)
		:effect
		(and
			( DEBARKING_P1_A0 )
			(not ( IN_P1_A0 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P1_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( IN_P1_A0 )
		)
		:effect
		(and
			( DEBARKING_P1_A0 )
			(not ( IN_P1_A0 ))
			(not ( NOT-DEBARKING_P1 ))
		)
	)
	(:action START-DEBARKING0_P0_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( IN_P0_A1 )
		)
		:effect
		(and
			( DEBARKING_P0_A1 )
			(not ( IN_P0_A1 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( IN_P0_A1 )
		)
		:effect
		(and
			( DEBARKING_P0_A1 )
			(not ( IN_P0_A1 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( IN_P0_A1 )
		)
		:effect
		(and
			( DEBARKING_P0_A1 )
			(not ( IN_P0_A1 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( IN_P0_A1 )
		)
		:effect
		(and
			( DEBARKING_P0_A1 )
			(not ( IN_P0_A1 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( IN_P0_A1 )
		)
		:effect
		(and
			( DEBARKING_P0_A1 )
			(not ( IN_P0_A1 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( IN_P0_A1 )
		)
		:effect
		(and
			( DEBARKING_P0_A1 )
			(not ( IN_P0_A1 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( IN_P0_A0 )
		)
		:effect
		(and
			( DEBARKING_P0_A0 )
			(not ( IN_P0_A0 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( IN_P0_A0 )
		)
		:effect
		(and
			( DEBARKING_P0_A0 )
			(not ( IN_P0_A0 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( IN_P0_A0 )
		)
		:effect
		(and
			( DEBARKING_P0_A0 )
			(not ( IN_P0_A0 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( IN_P0_A0 )
		)
		:effect
		(and
			( DEBARKING_P0_A0 )
			(not ( IN_P0_A0 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( IN_P0_A0 )
		)
		:effect
		(and
			( DEBARKING_P0_A0 )
			(not ( IN_P0_A0 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action START-DEBARKING0_P0_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( IN_P0_A0 )
		)
		:effect
		(and
			( DEBARKING_P0_A0 )
			(not ( IN_P0_A0 ))
			(not ( NOT-DEBARKING_P0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( BOARDING_P1_A1 )
		)
		:effect
		(and
			( IN_P1_A1 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( BOARDING_P1_A1 )
		)
		:effect
		(and
			( IN_P1_A1 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( BOARDING_P1_A1 )
		)
		:effect
		(and
			( IN_P1_A1 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( BOARDING_P1_A1 )
		)
		:effect
		(and
			( IN_P1_A1 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( BOARDING_P1_A1 )
		)
		:effect
		(and
			( IN_P1_A1 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( BOARDING_P1_A1 )
		)
		:effect
		(and
			( IN_P1_A1 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( BOARDING_P1_A0 )
		)
		:effect
		(and
			( IN_P1_A0 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( BOARDING_P1_A0 )
		)
		:effect
		(and
			( IN_P1_A0 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( BOARDING_P1_A0 )
		)
		:effect
		(and
			( IN_P1_A0 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( BOARDING_P1_A0 )
		)
		:effect
		(and
			( IN_P1_A0 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( BOARDING_P1_A0 )
		)
		:effect
		(and
			( IN_P1_A0 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P1_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( BOARDING_P1_A0 )
		)
		:effect
		(and
			( IN_P1_A0 )
			( NOT-BOARDING_P1 )
			(not ( BOARDING_P1_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A1_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C5 )
			( BOARDING_P0_A1 )
		)
		:effect
		(and
			( IN_P0_A1 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( BOARDING_P0_A1 )
		)
		:effect
		(and
			( IN_P0_A1 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( BOARDING_P0_A1 )
		)
		:effect
		(and
			( IN_P0_A1 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A1_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C2 )
			( BOARDING_P0_A1 )
		)
		:effect
		(and
			( IN_P0_A1 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A1_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C1 )
			( BOARDING_P0_A1 )
		)
		:effect
		(and
			( IN_P0_A1 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A1_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C0 )
			( BOARDING_P0_A1 )
		)
		:effect
		(and
			( IN_P0_A1 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A1 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A0_C5
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C5 )
			( BOARDING_P0_A0 )
		)
		:effect
		(and
			( IN_P0_A0 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( BOARDING_P0_A0 )
		)
		:effect
		(and
			( IN_P0_A0 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( BOARDING_P0_A0 )
		)
		:effect
		(and
			( IN_P0_A0 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A0_C2
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C2 )
			( BOARDING_P0_A0 )
		)
		:effect
		(and
			( IN_P0_A0 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A0_C1
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C1 )
			( BOARDING_P0_A0 )
		)
		:effect
		(and
			( IN_P0_A0 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A0 ))
		)
	)
	(:action COMPLETE-BOARDING0_P0_A0_C0
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C0 )
			( BOARDING_P0_A0 )
		)
		:effect
		(and
			( IN_P0_A0 )
			( NOT-BOARDING_P0 )
			(not ( BOARDING_P0_A0 ))
		)
	)
	(:action START-BOARDING0_P1_A1_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C4 )
			( AT-PERSON_P1_C4 )
		)
		:effect
		(and
			( BOARDING_P1_A1 )
			(not ( AT-PERSON_P1_C4 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P1_A0_C4
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C4 )
			( AT-PERSON_P1_C4 )
		)
		:effect
		(and
			( BOARDING_P1_A0 )
			(not ( AT-PERSON_P1_C4 ))
			(not ( NOT-BOARDING_P1 ))
		)
	)
	(:action START-BOARDING0_P0_A1_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A1_C3 )
			( AT-PERSON_P0_C3 )
		)
		:effect
		(and
			( BOARDING_P0_A1 )
			(not ( AT-PERSON_P0_C3 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action START-BOARDING0_P0_A0_C3
		:parameters ()
		:precondition
		(and
			( AT-AIRCRAFT_A0_C3 )
			( AT-PERSON_P0_C3 )
		)
		:effect
		(and
			( BOARDING_P0_A0 )
			(not ( AT-PERSON_P0_C3 ))
			(not ( NOT-BOARDING_P0 ))
		)
	)
	(:action COMPLETE-REFULING0_A1_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( REFUELING_A1 )
		)
		:effect
		(and
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F1 )
			(not ( REFUELING_A1 ))
			(not ( FUEL-LEVEL_A1_F0 ))
		)
	)
	(:action COMPLETE-REFULING0_A0_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( REFUELING_A0 )
		)
		:effect
		(and
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			(not ( REFUELING_A0 ))
			(not ( FUEL-LEVEL_A0_F0 ))
		)
	)
	(:action COMPLETE-REFULING0_A1_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( REFUELING_A1 )
		)
		:effect
		(and
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			(not ( REFUELING_A1 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-REFULING0_A0_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( REFUELING_A0 )
		)
		:effect
		(and
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F2 )
			(not ( REFUELING_A0 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-REFULING0_A1_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( REFUELING_A1 )
		)
		:effect
		(and
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			(not ( REFUELING_A1 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-REFULING0_A1_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( REFUELING_A1 )
		)
		:effect
		(and
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			(not ( REFUELING_A1 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action START-REFUELING0_A1_C5_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C4_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C3_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C2_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C1_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C0_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F0 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A0_C5_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C4_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C3_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C2_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C1_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C0_F0_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F0 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A1_C5_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C4_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C3_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C2_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C1_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C0_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A0_C5_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C5 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C4_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C4 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C3_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C3 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C2_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C2 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C1_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A0_C0_F1_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( NOT-REFUELING_A0 )
			( AT-AIRCRAFT_A0_C0 )
		)
		:effect
		(and
			( REFUELING_A0 )
			(not ( NOT-REFUELING_A0 ))
		)
	)
	(:action START-REFUELING0_A1_C5_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C4_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C3_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C2_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C1_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C0_F2_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C5_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C4_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C3_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C2_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C1_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action START-REFUELING0_A1_C0_F3_F4
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( NOT-REFUELING_A1 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( REFUELING_A1 )
			(not ( NOT-REFUELING_A1 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( ZOOMING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F0 )
			(not ( ZOOMING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( ZOOMING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F0 )
			(not ( ZOOMING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( ZOOMING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F0 )
			(not ( ZOOMING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( ZOOMING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F0 )
			(not ( ZOOMING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( ZOOMING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F0 )
			(not ( ZOOMING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( ZOOMING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F0 )
			(not ( ZOOMING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( ZOOMING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F1 )
			(not ( ZOOMING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( ZOOMING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F1 )
			(not ( ZOOMING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( ZOOMING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F1 )
			(not ( ZOOMING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( ZOOMING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F1 )
			(not ( ZOOMING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( ZOOMING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F1 )
			(not ( ZOOMING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( ZOOMING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F1 )
			(not ( ZOOMING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( ZOOMING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F2 )
			(not ( ZOOMING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( ZOOMING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F2 )
			(not ( ZOOMING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( ZOOMING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F2 )
			(not ( ZOOMING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( ZOOMING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F2 )
			(not ( ZOOMING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( ZOOMING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F2 )
			(not ( ZOOMING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-ZOOMING0_A1_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( ZOOMING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F2 )
			(not ( ZOOMING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C5_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C4_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C3_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C2_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C1_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C0_F2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F2 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C5_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C4_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C3_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C2_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C1_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C0_F3_F2_F1
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F3 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C5_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C5 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C5 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C4_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C4 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C4 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C3_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C3 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C3 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C2_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C2 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C2 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C1_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C1 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C1 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C5_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C4_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C3_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C2_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C1_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-ZOOMING0_A1_C0_C0_F4_F3_F2
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( ZOOMING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( FLYING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F0 )
			(not ( FLYING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( FLYING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F0 )
			(not ( FLYING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( FLYING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F0 )
			(not ( FLYING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( FLYING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F0 )
			(not ( FLYING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( FLYING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F0 )
			(not ( FLYING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F1 )
			( FLYING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F0 )
			(not ( FLYING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( FLYING_A0_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C5 )
			( FUEL-LEVEL_A0_F0 )
			(not ( FLYING_A0_C5 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( FLYING_A0_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C4 )
			( FUEL-LEVEL_A0_F0 )
			(not ( FLYING_A0_C4 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( FLYING_A0_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C3 )
			( FUEL-LEVEL_A0_F0 )
			(not ( FLYING_A0_C3 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( FLYING_A0_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C2 )
			( FUEL-LEVEL_A0_F0 )
			(not ( FLYING_A0_C2 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( FLYING_A0_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C1 )
			( FUEL-LEVEL_A0_F0 )
			(not ( FLYING_A0_C1 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A0_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A0_F1 )
			( FLYING_A0_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A0_C0 )
			( FUEL-LEVEL_A0_F0 )
			(not ( FLYING_A0_C0 ))
			(not ( FUEL-LEVEL_A0_F1 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C5_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( FLYING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F1 )
			(not ( FLYING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C4_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( FLYING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F1 )
			(not ( FLYING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C3_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( FLYING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F1 )
			(not ( FLYING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C2_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( FLYING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F1 )
			(not ( FLYING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C1_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( FLYING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F1 )
			(not ( FLYING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C0_F2_F1
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F2 )
			( FLYING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F1 )
			(not ( FLYING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F2 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C5_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( FLYING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F2 )
			(not ( FLYING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C4_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( FLYING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F2 )
			(not ( FLYING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C3_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( FLYING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F2 )
			(not ( FLYING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C2_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( FLYING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F2 )
			(not ( FLYING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C1_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( FLYING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F2 )
			(not ( FLYING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C0_F3_F2
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F3 )
			( FLYING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F2 )
			(not ( FLYING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F3 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( FLYING_A1_C5 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C5 )
			( FUEL-LEVEL_A1_F3 )
			(not ( FLYING_A1_C5 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( FLYING_A1_C4 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C4 )
			( FUEL-LEVEL_A1_F3 )
			(not ( FLYING_A1_C4 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( FLYING_A1_C3 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C3 )
			( FUEL-LEVEL_A1_F3 )
			(not ( FLYING_A1_C3 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( FLYING_A1_C2 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C2 )
			( FUEL-LEVEL_A1_F3 )
			(not ( FLYING_A1_C2 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( FLYING_A1_C1 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C1 )
			( FUEL-LEVEL_A1_F3 )
			(not ( FLYING_A1_C1 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action COMPLETE-FLYING0_A1_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( FUEL-LEVEL_A1_F4 )
			( FLYING_A1_C0 )
		)
		:effect
		(and
			( AT-AIRCRAFT_A1_C0 )
			( FUEL-LEVEL_A1_F3 )
			(not ( FLYING_A1_C0 ))
			(not ( FUEL-LEVEL_A1_F4 ))
		)
	)
	(:action START-FLYING0_A0_C1_C5_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C5 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C4_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C4 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C3_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C3 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C2_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C2 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C1_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C1 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A0_C1_C0_F1_F0
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A0 )
			( FUEL-LEVEL_A0_F1 )
			( AT-AIRCRAFT_A0_C1 )
		)
		:effect
		(and
			( FLYING_A0_C0 )
			(not ( AT-AIRCRAFT_A0_C1 ))
		)
	)
	(:action START-FLYING0_A1_C0_C5_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C5 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C4_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C4 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C3_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C3 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C2_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C2 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C1_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C1 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)
	(:action START-FLYING0_A1_C0_C0_F4_F3
		:parameters ()
		:precondition
		(and
			( NOT-BOARDING_P0 )
			( NOT-DEBARKING_P0 )
			( NOT-BOARDING_P1 )
			( NOT-DEBARKING_P1 )
			( NOT-REFUELING_A1 )
			( FUEL-LEVEL_A1_F4 )
			( AT-AIRCRAFT_A1_C0 )
		)
		:effect
		(and
			( FLYING_A1_C0 )
			(not ( AT-AIRCRAFT_A1_C0 ))
		)
	)

)
