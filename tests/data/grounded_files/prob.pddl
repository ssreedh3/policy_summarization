(define
	(problem grounded-ZENO_6_2_2_3846)
	(:domain grounded-ZENOTRAVEL)
	(:init
		( NOT-REFUELING_A1 )
		( FUEL-LEVEL_A1_F4 )
		( AT-AIRCRAFT_A1_C0 )
		( NOT-REFUELING_A0 )
		( FUEL-LEVEL_A0_F1 )
		( AT-AIRCRAFT_A0_C1 )
		( NOT-DEBARKING_P1 )
		( NOT-BOARDING_P1 )
		( AT-PERSON_P1_C4 )
		( NOT-DEBARKING_P0 )
		( NOT-BOARDING_P0 )
		( AT-PERSON_P0_C3 )
	)
	(:goal
		(and 
		( AT-PERSON_P1_C4 )
		( AT-PERSON_P0_C3 )
		)
	)

)
